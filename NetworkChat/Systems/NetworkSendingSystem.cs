﻿using System;
using System.Diagnostics;
using System.Threading;
using NetworkChat.Configuration;
using NetworkChat.Core;
using NetworkChat.Logic;
using NetworkChat.Networking;
using NetworkChatTypes.Messages;
using NetworkChatTypes.Utils;

namespace NetworkChat.Systems
{
  /// <summary>
  /// Depends on CPU this class run its own thread to sends outgoing requests to clients or it is updated from main thread.
  /// </summary>
  internal class NetworkSendingSystem
  {
    private readonly IChatLogger _logger;
    private readonly NetworkClients _networkClients;
    private readonly OutgoingTcpRequests _outgoingTcpRequests;
    private readonly int _capacity;

    public NetworkSendingSystem(NetworkClients networkClients,
      OutgoingTcpRequests outgoingTcpRequests, 
      IChatLogger logger, 
      ChatConfiguration configuration)
    {
      _networkClients = networkClients;
      _outgoingTcpRequests = outgoingTcpRequests;
      _logger = logger;
      _capacity = configuration.ServerCapacity;
    }

    /// <summary>
    /// If chat server can have >= 4 threads than we can set up separated listening thread.
    /// </summary>
    public void StartThreaded()
    {
      ProceedClients(_capacity);
      _logger.Log("Network sending system started.");
    }

    /// <summary>
    /// If chat server CAN'T have 4 or more threads than we should call this update from the main thread.
    /// </summary>
    public void Update()
    {
      for (int i = 0; i < _capacity; i++)
      {
        if (_networkClients.Records[i].Client != null)
        {
          SendOutgoingRequests(i, ref _networkClients.Records[i]);
        }
      }
    }

    private void ProceedClients(int serverCapacity)
    {
      new Thread(() => ProceedClients(0, serverCapacity))
      {
        IsBackground = true
      }.Start();
    }

    private void ProceedClients(int start, int end)
    {
      Stopwatch stopwatch = Stopwatch.StartNew();
      while (ChatServer.IsRunning)
      {
        for (int i = start; i < end; i++)
        {
          if (_networkClients.Records[i].Client != null)
          {
            SendOutgoingRequests(i, ref _networkClients.Records[i]);
          }
        }
        stopwatch.Stop();
        long milliseconds = 33 - stopwatch.ElapsedMilliseconds;
        if (milliseconds > 0)
          Thread.Sleep((int)milliseconds);
        stopwatch.Restart();
      }
    }

    private void SendOutgoingRequests(int index, ref NetworkClientRecord record)
    {
      try
      {
        if (!record.Client.IsConnected)
        {
          _networkClients.Records[index].Client.Dispose();
          _networkClients.Records[index] = new NetworkClientRecord();
          return;
        }

        if (_outgoingTcpRequests.TryDequeue(index, out NetworkRequest[] result))
        {
          foreach (NetworkRequest networkRequest in result)
          {
            record.Client.Send(networkRequest.Serialize());
          }
        }
      }
      catch
      {
        _networkClients.Records[index].Client.Dispose();
        _networkClients.Records[index] = new NetworkClientRecord();
      }
    }
  }
}
