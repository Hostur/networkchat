﻿using System;
using System.Diagnostics;
using System.Threading;
using NetworkChat.Configuration;
using NetworkChat.Core;
using NetworkChat.Networking;
using NetworkChat.Networking.Requests;
using NetworkChatTypes.Messages;
using NetworkChatTypes.Utils;

namespace NetworkChat.Systems
{
  /// <summary>
  /// Depends on CPU this class run its own thread to listening incoming requests from clients or it is updated from main thread.
  /// </summary>
  internal class NetworkListeningSystem
  {
    private readonly IChatLogger _logger;
    private readonly NetworkClients _networkClients;
    private readonly GenericServerRequestHandler _genericServerRequestHandler;
    private readonly int _serverCapacity;

    public NetworkListeningSystem(
      NetworkClients networkClients,
      GenericServerRequestHandler genericServerRequestHandler, IChatLogger logger, ChatConfiguration configuration)
    {
      _networkClients = networkClients;
      _genericServerRequestHandler = genericServerRequestHandler;
      _logger = logger;
      _serverCapacity = configuration.ServerCapacity;
    }

    /// <summary>
    /// If chat server can have >= 4 threads than we can set up separated listening thread.
    /// </summary>
    public void StartThreaded()
    {
      ProceedClients();
      _logger.Log("Network listening system started.");
    }

    /// <summary>
    /// If chat server CAN'T have 4 or more threads than we should call this update from the main thread.
    /// </summary>
    public void Update()
    {
      for (int i = 0; i < _serverCapacity; i++)
      {
        if (_networkClients.Records[i].Client != null)
        {
          ListenClient(i, _networkClients.Records[i]);
        }
      }
    }

    private void ProceedClients()
    {
      new Thread(() => ProceedClients(0, _serverCapacity))
      {
        IsBackground = true
      }.Start();
    }

    private void ProceedClients(int start, int end)
    {
      Stopwatch stopwatch = Stopwatch.StartNew();
      while (ChatServer.IsRunning)
      {
        for (int i = start; i < end; i++)
        {
          if (_networkClients.Records[i].Client != null)
          {
            ListenClient(i, _networkClients.Records[i]);
          }
        }
        stopwatch.Stop();
        long milliseconds = 33 - stopwatch.ElapsedMilliseconds;
        if (milliseconds > 0)
          Thread.Sleep((int)milliseconds);
        stopwatch.Restart();
      }
    }

    private void ListenClient(int index, NetworkClientRecord record)
    {
      if (!record.Client.IsConnected)
      {
        _networkClients.Records[index].Client.Dispose();
        _networkClients.Records[index] = new NetworkClientRecord();
        return;
      }

      try
      {
        if (record.Client.Receive(out byte[] data))
        {
          var request = data.Deserialize<NetworkRequest>();
          HandleIncomingData(index, ref record, ref request);
        }
      }
      catch(Exception e)
      {
        _logger.LogError("Exception occur during reading from client stream: " + e.Message);
        _networkClients.Records[index].Client.Dispose();
        _networkClients.Records[index] = new NetworkClientRecord();
        //_logger.Log("ToDo: Implement disconnecting");
      }
    }

    private void HandleIncomingData(int index, ref NetworkClientRecord record, ref NetworkRequest request)
    {
      try
      {
        _genericServerRequestHandler.HandleClientRequest(index, record.StringClientIdentifier, ref request);
      }
      catch (Exception e)
      {
        _logger.LogError("Handle incoming data exception: " + e);
      }
    }
  }
}