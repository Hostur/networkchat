﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using NetworkChat.Configuration;
using NetworkChat.Core;
using NetworkChat.Networking;
using NetworkChat.Systems;
using Open.Nat;
using PlayFab;
using PlayFab.ClientModels;

namespace NetworkChat
{
  internal class ChatServer
  {
    public static bool IsRunning;
    private readonly ServerConfiguration _serverConfiguration;
    private readonly NetworkListener _networkListener;
    private readonly NetworkSendingSystem _networkSendingSystem;
    private readonly NetworkListeningSystem _networkListeningSystem;
    private readonly IChatLogger _logger;
    private readonly ChatConfiguration _chatConfiguration;

    public static ServerConfigurationData ConfigurationData;

    public ChatServer(ServerConfiguration serverConfiguration,
      NetworkListener networkListener,
      NetworkSendingSystem networkSendingSystem,
      NetworkListeningSystem networkListeningSystem,
      IChatLogger logger,
      ChatConfiguration chatConfiguration)
    {
      _serverConfiguration = serverConfiguration;
      _networkListener = networkListener;
      _networkSendingSystem = networkSendingSystem;
      _networkListeningSystem = networkListeningSystem;
      _logger = logger;
      _chatConfiguration = chatConfiguration;
    }

    public async Task Start(int physicalCPUCoresQuantity)
    {
      Console.WriteLine("Started.");
      IsRunning = true;
      try
      {
        ConfigurationData = _serverConfiguration.InitConfiguration(_chatConfiguration);
        if (ConfigurationData.ForwardPort)
        {
          int port = await Networking.PortForwarding.NATUPnP.CreateMapping(ConfigurationData.ServerHostPort);
          if (port != ConfigurationData.ServerHostPort)
          {
            throw new Exception(
              $"Cannot forward server host port '{ConfigurationData.ServerHostPort}' as a public one. \n" +
              $"Port was forwarded as '{port}'. Change default application port in App.config or ignore this exception \n" +
              $"and provide forwarded port to your chat clients.");
          }
        }

        IsRunning = true;
        // Start listening for incoming clients.
        _networkListener.Start(ConfigurationData);

        // If there are more than 2 CPU cores we can start separated threads
        if (physicalCPUCoresQuantity >= 2)
        {
            _networkListeningSystem.StartThreaded();
            _networkSendingSystem.StartThreaded();
        }
        // Otherwise just run listening and sending system update from the main thread.
        else
        {
          Stopwatch stopwatch = Stopwatch.StartNew();
          while (IsRunning)
          {
            _networkListeningSystem.Update();
            _networkSendingSystem.Update();
            stopwatch.Stop();
            long milliseconds = 33 - stopwatch.ElapsedMilliseconds;
            if (milliseconds > 0)
              Thread.Sleep((int)milliseconds);
            stopwatch.Restart();
          }
        }
      }
      catch (NatDeviceNotFoundException e)
      {
        _logger.LogError("Port forwarding failed. " + e);
        IsRunning = false;
      }
      catch (Exception e)
      {
        IsRunning = false;
        _logger.LogError("Exception occur during starting ChatServer. " + e.Message);
      }
    }
  }
}
