﻿using System;
using NetworkChat.Configuration;
using NetworkChatTypes.Logic.Requests;
using NetworkChatTypes.Messages;

namespace NetworkChat.Logic
{
  /// <summary>
  /// Class which contains outgoing tcp requests for each chat client.
  /// Based on NetworkClients index to avoid searching for players while iterate through all the clients from sending/listening threads.
  /// </summary>
  internal class OutgoingTcpRequests
  {
    private readonly OutgoingNetworkRequestsQueue[] _queues;

    public OutgoingTcpRequests(ChatConfiguration configuration)
    {
      _queues = new OutgoingNetworkRequestsQueue[configuration.ServerCapacity];
      for (int i = 0; i < configuration.ServerCapacity; i++)
      {
        _queues[i] = new OutgoingNetworkRequestsQueue(4);
      }
    }

    public void Clear(int index)
    {
      _queues[index].Clear();
    }

    public void Enqueue(int internalIdentifier, NetworkRequest request)
    {
      _queues[internalIdentifier].EnqueueRequest(request);
    }

    public bool TryDequeue(int internalIdentifier, out NetworkRequest[] result)
    {
      return _queues[internalIdentifier].TryDequeueRequests(out result);
    }

    /// <summary>
    /// Use internal identifier to retrieve <see cref="OutgoingNetworkRequestsQueue"/>.
    /// </summary>
    /// <param name="index">Internal index from Network clients.</param>
    /// <returns>Queue with outgoing network tcp requests.</returns>
    public OutgoingNetworkRequestsQueue this[int index] => _queues[index];
  }
}
