﻿using System;
using System.Collections.Generic;
using NetworkChat.Configuration;
using NetworkChat.Networking;
using NetworkChatTypes.Messages;

namespace NetworkChat.Logic.Channels
{
  internal class PrivateChannels
  {
    private ServerPrivetChannel[] _channels;
    private OutgoingTcpRequests _outgoingTcpRequests;
    private NetworkClients _networkClients;
    private int _lastIndex;

    public PrivateChannels(
      ChatConfiguration configuration, 
      OutgoingTcpRequests outgoingTcpRequests,
      NetworkClients networkClients)
    {
      _channels = new ServerPrivetChannel[configuration.ServerCapacity * configuration.ServerCapacity];
      _outgoingTcpRequests = outgoingTcpRequests;
      _networkClients = networkClients;
    }

    public int GetPrivateChannel(string clientA, string clientB)
    {
      int empty = -1;
      for (int i = 0; i < _lastIndex; i++)
      {
        if (_channels[i].IsFree)
        {
          empty = i;
          continue;
        }
        if (_channels[i].AreMembers(clientA, clientB))
        {
          return _channels[i].Index;
        }
      }
      // If there is no active channel for this pair and there was free channel found
      if (empty != -1)
      {
        _channels[empty].Assign(empty, clientA, clientB);
        return empty;
      }

      // No active channel for this pair and no empty channel so we have to create a new one.
      ++_lastIndex;
      _channels[_lastIndex].Assign(_lastIndex, clientA, clientB);
      return _lastIndex;
    }

    public List<string> GetChannelMessages(string clientA, string clientB)
    {
      return _channels[GetPrivateChannel(clientA, clientB)].GetMessages;
    }

    public List<string> GetChannelMessages(int channel, string client)
    {
      if (_channels[channel].IsMember(client))
      {
        return _channels[channel].GetMessages;
      }
      return null;
    }

    public string OnNewMessage(int channelIdentifier, string sender, string message)
    {
      // If sender is a member of given channel identifier.
      if (_channels[channelIdentifier].IsMember(sender))
      {
        string receiver = (_channels[channelIdentifier].ClientA == sender)
          ? _channels[channelIdentifier].ClientB
          : _channels[channelIdentifier].ClientA;
        _channels[channelIdentifier].AddMessage(sender, message);
        return receiver;
      }
      // Otherwise it mean that player hold channel identifier locally for so long that server cleaned it.
      // So this client should create a new channel.
      else
      {
        int index = _networkClients.GetIndexByIdentifier(sender);
        if(index == -1) throw new Exception("Sender is not valid - not found in network clients by its identifier.");
        _outgoingTcpRequests[index].EnqueueRequest(new InvalidChannelRequest(channelIdentifier).ToMessage());
        return null;
      }
    }

    //public string GetTargetPlayerByChannelIdentifier(int channelId, string sender, string message)
    //{
    //  _channels[channelId].Messages.Add(message);
    //  return (_channels[channelId].ClientA == sender) ? _channels[channelId].ClientB : _channels[channelId].ClientA;
    //}
  }
}
