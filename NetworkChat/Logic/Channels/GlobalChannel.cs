﻿using System;
using NetworkChat.Configuration;
using NetworkChat.Networking;
using NetworkChatTypes.Messages;

namespace NetworkChat.Logic.Channels
{
    internal sealed class GlobalChannel
    {
      private readonly FixedLengthCollection _messages;
      private readonly ChatConfiguration _chatConfiguration;
      private readonly OutgoingTcpRequests _outgoingTcpRequests;
      private readonly NetworkClients _networkClients;

      /// <summary>
      /// This array hold last activity of each chat client.
      /// Depends on App.config param this datetime protect global channel from being spammed.
      /// </summary>
      private readonly DateTime[] _lastActivity;

      public GlobalChannel(
        ChatConfiguration configuration,
        ChatConfiguration chatConfiguration, NetworkClients networkClients, OutgoingTcpRequests outgoingTcpRequests)
      {
        _chatConfiguration = chatConfiguration;
        _networkClients = networkClients;
        _outgoingTcpRequests = outgoingTcpRequests;
        _messages = new FixedLengthCollection();
        _lastActivity = new DateTime[configuration.ServerCapacity];
        for (int i = 0; i < configuration.ServerCapacity; i++)
        {
          _lastActivity[i] = DateTime.MinValue;
        }
      }

      public void OnNewMessage(int index, string identifier, string message)
      {
        const string MESSAGE = "{0}:{1}";
        TimeSpan duration = new TimeSpan(DateTime.Now.Minute - _lastActivity[index].Minute);
        if (duration > TimeSpan.FromMinutes(_chatConfiguration.GlobalChatCooldown))
        {
          var strMsg = string.Format(MESSAGE, identifier, message);
          _messages.Add(strMsg);

          var msg = new ChatMessage {ChannelIdentifier = -1, Message = strMsg}.ToMessage();
          // Push msg to each active client.
          foreach (int id in _networkClients.ForEachIndex())
          {
            _outgoingTcpRequests.Enqueue(id, msg);
          }
        }
      }

      public ChannelOpenedRequest GetChatOpenedRequest(string identifier)
      {
        return new ChannelOpenedRequest
        {
          ChannelIdentifier = -1,
          ClientA = identifier,
          ClientB = "global",
          Messages = _messages.GetMessages(),
          TypeOfChannel = ChannelType.Global
        };
      }

      /// <summary>
      /// Takes global chat content and push it to given client channel.
      /// </summary>
      /// <param name="clientIndex">Client clientIndex.</param>
      /// <param name="clientStringIdentifier">Client clientStringIdentifier.</param>
      public void PushGlobalContentToClient(int clientIndex, string clientStringIdentifier)
      {
        var msg = new ChannelOpenedRequest
        {
          ChannelIdentifier = -1,
          ClientA = clientStringIdentifier,
          ClientB = "global",
          Messages = _messages.GetMessages(),
          TypeOfChannel = ChannelType.Global
        };
        _outgoingTcpRequests.Enqueue(clientIndex, msg.ToMessage());
      }
    }
}
