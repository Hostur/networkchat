﻿using System;
using System.Collections.Generic;

namespace NetworkChat.Logic.Channels
{
  public struct ServerPrivetChannel
  {
    private const string FORMAT_MESSAGE = "{0}: {1}";
    public int Index { get; set; }
    public string ClientA { get; set; }
    public string ClientB { get; set; }
    private IMessagesStack Messages { get; set; }
    public DateTime LastActivity { get; set; }
    public bool IsFree { get; set; }

    public void AddMessage(string sender, string message)
    {
      Messages.Add(string.Format(FORMAT_MESSAGE, sender, message));
      LastActivity = DateTime.Now;
    }

    public List<string> GetMessages => Messages?.GetMessages();

    public void Assign(int index, string clientA, string clientB)
    {
      Index = index;
      ClientA = clientA;
      ClientB = clientB;
      if (Messages == null) Messages = MessagesStackFactory.Create();
      IsFree = false;
      LastActivity = DateTime.Now;
      // ToDo handle history?
    }

    public void Release()
    {
      // ToDo handle history?
      Index = -1;
      ClientA = string.Empty;
      ClientB = string.Empty;
      Messages.Clear();
      IsFree = true;
    }

    public bool IsMember(string client)
    {
      if (string.IsNullOrEmpty(client)) return false;
      return client == ClientA || client == ClientB;
    }

    public bool AreMembers(string clientA, string clientB)
    {
      if (string.IsNullOrEmpty(clientA) || string.IsNullOrEmpty(clientB)) return false;
      if (clientA == ClientA)
      {
        return clientB == ClientB;
      }

      return ClientA == ClientB && clientB == ClientA;
    }
  }
}
