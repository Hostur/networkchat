﻿using System;
using System.Collections.Generic;

namespace NetworkChat.Logic
{
  /// <summary>
  /// Collection that can remain in given capacity range.
  /// In additional it provides changed event which is triggered after each new message.
  /// </summary>
  internal class FixedLengthCollection : IMessagesStack
  {
    private readonly int _capacity;
    private readonly List<string> _messages;

    public FixedLengthCollection(int maxCapacity = 50)
    {
      _capacity = maxCapacity;
      _messages = new List<string>(maxCapacity);
    }

    /// <summary>
    /// Add new message to stack.
    /// </summary>
    /// <param name="message">Message to add.</param>
    public void Add(string message)
    {
      if (_messages.Count >= _capacity)
      {
        _messages.RemoveAt(0);
      }
      _messages.Add(message);
    }

    public void Clear()
    {
      _messages.Clear();
    }

    /// <summary>
    /// Gets messages from the top.
    /// </summary>
    /// <param name="count">Messages quantity.</param>
    /// <returns>Messages.</returns>
    public List<string> GetMessages(int count)
    {
      List<string> result = new List<string>(count);
      int counter = 0;
      for (int i = _messages.Count - 1; i >= 0 && ++counter <= count; i--)
      {
        result.Add(_messages[i]);
      }

      return result;
    }

    /// <summary>
    /// Gets messages from the top.
    /// </summary>
    /// <returns>Messages.</returns>
    public List<string> GetMessages()
    {
      List<string> result = new List<string>(_messages.Count);
      for (int i = _messages.Count - 1; i >= 0; i--)
      {
        result.Add(_messages[i]);
      }

      return result;
    }

    public int Count => _messages.Count;
  }

  public interface IMessagesStack
  {
    List<string> GetMessages(int count);
    List<string> GetMessages();
    void Add(string message);
    void Clear();
    int Count { get; }
  }

  public static class MessagesStackFactory
  {
    public static IMessagesStack Create() => new FixedLengthCollection();
  }
}
