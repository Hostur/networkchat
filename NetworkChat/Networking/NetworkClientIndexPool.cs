﻿using System;
using System.Collections.Generic;
using NetworkChat.Configuration;

namespace NetworkChat.Networking
{
  internal class NetworkClientIndexPool
  {
    private readonly int _capacity;
    private readonly Stack<int> _availableIdentifiers;

    public NetworkClientIndexPool(ChatConfiguration configuration)
    {
      _capacity = configuration.ServerCapacity;
      _availableIdentifiers = new Stack<int>(_capacity);
      for (int i = 0; i < _capacity; i++)
      {
        _availableIdentifiers.Push(i);
      }
    }

    public bool GetFirstAvailable(out int result)
    {
      lock (_availableIdentifiers)
      {
        if (_availableIdentifiers.Count > 0)
        {
          result = _availableIdentifiers.Pop();
          return true;
        }
        result = 0;
        return false;
      }
    }

     public void PushBackFreeIdentifierForPlayer(int identifier)
    {
      lock (_availableIdentifiers)
      {
        _availableIdentifiers.Push(identifier);
      }
    }
  }
}
