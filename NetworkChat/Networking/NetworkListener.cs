﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using NetworkChat.Configuration;
using NetworkChat.Core;
using NetworkChatTypes.Clients;
using NetworkChatTypes.Messages;
using NetworkChatTypes.Utils;
using PlayFab;
using PlayFab.ServerModels;

namespace NetworkChat.Networking
{
  internal class NetworkListener
  {
    private TcpListener _tcpListener;
    private readonly IChatLogger _chatLogger;
    private readonly List<INetworkClient> _clients;
    private readonly NetworkClients _networkClients;
    private readonly Thread _readingUnconfirmedClientsThread;

    public NetworkListener(IChatLogger chatLogger, NetworkClients networkClients)
    {
      _chatLogger = chatLogger;
      _networkClients = networkClients;
      _clients = new List<INetworkClient>(100);
      _readingUnconfirmedClientsThread = new Thread(ReadingFromUnconfirmedSocketsUpdate) {IsBackground = false};
    }

    public void Start(ServerConfigurationData config)
    {
      try
      {
        _chatLogger.Log($"Opening tcp listener on ip: {config.ServerHostDomain} : {config.ServerHostPort} ");
        
        //IPAddress ip_address = config.IsPlayFab ? IPAddress.Parse(config.ServerHostDomain) : IPAddress.Any;
        //_tcpListener = new TcpListener(ip_address, config.ServerHostPort);
        _tcpListener = new TcpListener(IPAddress.Any, config.ServerHostPort);
        _tcpListener.Start();
        _readingUnconfirmedClientsThread.Start();
        StartListening();
      }
      catch (Exception e)
      {
        _chatLogger.LogError("Error occur during opening TcpListener. " + e);
      }
    }

    private void StartListening()
    {
      _tcpListener?.BeginAcceptTcpClient(AcceptTcpClient, _tcpListener);
    }

    private void AcceptTcpClient(IAsyncResult asyncResult)
    {
      try
      {
        Console.WriteLine("Client connected..");
        TcpListener listener = (TcpListener)asyncResult.AsyncState;
        TcpClient tmpTcpClient = listener.EndAcceptTcpClient(asyncResult);

        OnClientConnected(tmpTcpClient);
      }
      catch (Exception e)
      {
        _chatLogger.LogError("Exception occur during AcceptTcpClient. " + e.Message);
      }
      finally
      {
        StartListening();
      }
    }

    public void ReadingFromUnconfirmedSocketsUpdate()
    {
      _chatLogger.Log("Started reading from unconfirmed clients..");
      while (ChatServer.IsRunning)
      {
        for (int i = 0; i < _clients.Count; i++)
        {
          if (ListenNonConfirmedClients(_clients[i]))
          {
            _clients.RemoveAt(i);
            if (i > 0)
            {
              i--;
            }
          }
        }
        Thread.Sleep(1000);
      }
    }

    private void OnClientConnected(TcpClient client)
    {
      try
      {
        INetworkClient networkClient = NetworkClientsFactory.Create(client);
        _clients.Add(networkClient);
        networkClient.Send(new AuthorizationRequest { Status = Status.ShouldConfirm }.ToRequest());
      }
      catch (Exception e)
      {
        _chatLogger.LogError("Exception occur during OnClientConnection: " + e);
      }
    }

    private bool ListenNonConfirmedClients(INetworkClient serverNetworkClient)
    {
      try
      {
        if (serverNetworkClient.Receive(out byte[] result))
        {
          var request = result.Deserialize<NetworkRequest>();
          if (request.RequestType == AuthorizationRequest.Type)
          {
            return OnClientConfirm(serverNetworkClient, request.RequestData.Deserialize<AuthorizationRequest>());
          }
          else
          {
            _chatLogger.LogError(
              "Bad confirm request from serverNetworkClient. " +
              "Probably serverNetworkClient sent something before authorization request.");
            _chatLogger.LogError("Bad request identifier: " + request.RequestType);
          }
      }

        return false;
      }
      catch (Exception e)
      {
        _chatLogger.LogError(e.ToString());
        return false;
      }
    }

    private bool OnClientConfirm(INetworkClient serverNetworkClient, AuthorizationRequest request)
    {
      Console.WriteLine("Client confirmed successfully..");

      if (!ChatServer.ConfigurationData.IsPlayFab)
      {
        serverNetworkClient.Send(new AuthorizationRequest { Status = Status.ConfirmedSuccessfully }.ToRequest());
        return _networkClients.Add(request.StringClientIdentifier, serverNetworkClient);
      }
      else
      {
        var result = PlayFabServerAPI.RedeemMatchmakerTicketAsync(new RedeemMatchmakerTicketRequest
        {
          Ticket = request.AuthenticationTicket,
          LobbyId = ChatServer.ConfigurationData.GameId.ToString()
        }).Result;
        if (result.Result.TicketIsValid)
        {
          serverNetworkClient.Send(new AuthorizationRequest {Status = Status.ConfirmedSuccessfully}.ToRequest());
          return _networkClients.Add(request.StringClientIdentifier, serverNetworkClient);
        }
        else
        {
          return false;
        }
      }
    }
  }
}
