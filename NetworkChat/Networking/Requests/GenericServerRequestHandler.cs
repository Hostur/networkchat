﻿using System;
using NetworkChat.Core;
using NetworkChatTypes.Messages;

namespace NetworkChat.Networking.Requests
{
  internal sealed class GenericServerRequestHandler
  {
    /// <summary>
    /// Dictionary that provides handler for every type of incoming request.
    /// </summary>
    private readonly ServerRequestsHandlersDictionary _handlersDictionary;

    private IChatLogger _chatLogger;

    /// <summary>
    /// <see cref="GenericServerRequestHandler"/> initialization.
    /// </summary>
    /// <param name="handlersDictionary">This dictionary is provides through Autofac.</param>
    public GenericServerRequestHandler(ServerRequestsHandlersDictionary handlersDictionary, IChatLogger chatLogger)
    {
      _handlersDictionary = handlersDictionary;
      _chatLogger = chatLogger;
    }

    /// <summary>
    /// Every incoming request should be passed into this function.
    /// This function map incoming request type to appropriate request handler.
    /// </summary>
    public void HandleClientRequest(int index, string networkClientIdentifier, ref NetworkRequest request)
    {
      try
      {
        IServerRequestHandler handler = _handlersDictionary[request.RequestType];
        if (handler == null)
        {
          Console.WriteLine("There is no request handler for request type: " + request.RequestType);
        }
        handler?.Handle(index, networkClientIdentifier, ref request);
      }
      catch (Exception e)
      {
        _chatLogger.LogError($"Handle client request exception of type '{request.RequestType}': '{e.Message}'.");
      }
    }
  }
}
