﻿using NetworkChat.Logic;
using NetworkChat.Logic.Channels;
using NetworkChatTypes.Messages;

namespace NetworkChat.Networking.Requests
{
  internal abstract class ServerRequestHandler : IServerRequestHandler
  {
    protected readonly NetworkClients Clients;
    protected readonly PrivateChannels PrivateChannels;
    protected readonly GlobalChannel GlobalChannel;
    protected readonly OutgoingTcpRequests OutgoingRequests;

    protected ServerRequestHandler(
      NetworkClients clients, 
      PrivateChannels privateChannels, 
      GlobalChannel globalChannel,
      OutgoingTcpRequests outgoingRequests)
    {
      Clients = clients;
      PrivateChannels = privateChannels;
      GlobalChannel = globalChannel;
      OutgoingRequests = outgoingRequests;
    }

    public abstract byte HandledType { get; }
    public abstract void Handle(int index, string identifier, ref NetworkRequest request);
  }
}
