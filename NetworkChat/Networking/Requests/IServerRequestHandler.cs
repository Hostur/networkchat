﻿using NetworkChatTypes.Messages;

namespace NetworkChat.Networking.Requests
{
  public interface IServerRequestHandler
  {
    byte HandledType { get; }
    void Handle(int index, string identifier, ref NetworkRequest request);
  }
}
