﻿using NetworkChat.Logic;
using NetworkChat.Logic.Channels;
using NetworkChatTypes.Messages;
using NetworkChatTypes.Utils;

namespace NetworkChat.Networking.Requests
{
    internal sealed class ChatMessagesHandler : ServerRequestHandler
    {
      public ChatMessagesHandler(NetworkClients clients, PrivateChannels privateChannels, GlobalChannel globalChannel, OutgoingTcpRequests outgoingRequests) : base(clients, privateChannels, globalChannel, outgoingRequests)
      {
      }

      public override byte HandledType => 0;
      public override void Handle(int index, string identifier, ref NetworkRequest request)
      {
        ChatMessage message = request.RequestData.Deserialize<ChatMessage>();
        switch (message.TypeOfChannel)
        {
                case ChannelType.Global:
                  GlobalChannel.OnNewMessage(index, identifier, message.Message);
                  break;
                case ChannelType.Group:
                  //ToDo
                  break;
                default:
                  OnPrivateChannelMessage(identifier, message);
                  break;
        }
      }

      private void OnPrivateChannelMessage(string identifier, ChatMessage message)
      {
        var receiver = PrivateChannels.OnNewMessage(message.ChannelIdentifier, identifier, message.Message);
        // It mean that receiver does not belong to this channel anymore.
        if (receiver == null) return;
        int receiverIndex = Clients.GetIndexByIdentifier(receiver);
        if (receiverIndex != -1)
        {
          OutgoingRequests.Enqueue(receiverIndex, message.ToMessage());
        }
      }
    }
}
