﻿using System;
using System.Threading.Tasks;
using NetworkChat.Logic;
using NetworkChat.Logic.Channels;
using NetworkChatTypes.Messages;
using PlayFab;
using PlayFab.ServerModels;

namespace NetworkChat.Networking.Requests
{
    internal sealed class DisconnectedRequestHandler : ServerRequestHandler
    {
      public DisconnectedRequestHandler(NetworkClients clients, PrivateChannels privateChannels, GlobalChannel globalChannel, OutgoingTcpRequests outgoingRequests) : base(clients, privateChannels, globalChannel, outgoingRequests)
      {
      }

      public override byte HandledType => 5;
      public override void Handle(int index, string identifier, ref NetworkRequest request)
      {
        Clients.Records[index].Client.Dispose();
        Clients.Records[index] = new NetworkClientRecord();
        OutgoingRequests.Clear(index);

        Task.Factory.StartNew(async () => await PlayFabServerAPI.NotifyMatchmakerPlayerLeftAsync(
          new NotifyMatchmakerPlayerLeftRequest
          {
            LobbyId = ChatServer.ConfigurationData.GameId.ToString(),
            PlayFabId = identifier
          }).ConfigureAwait(false)).ConfigureAwait(false);

        Console.WriteLine("Client disconnected.");
      }
    }
}
