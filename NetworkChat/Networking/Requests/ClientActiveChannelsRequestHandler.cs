﻿using System.Collections.Generic;
using NetworkChat.Logic;
using NetworkChat.Logic.Channels;
using NetworkChatTypes.Messages;
using NetworkChatTypes.Utils;

namespace NetworkChat.Networking.Requests
{
    internal sealed class ClientActiveChannelsRequestHandler : ServerRequestHandler
    {
      public ClientActiveChannelsRequestHandler(
        NetworkClients clients,
        PrivateChannels privateChannels,
        GlobalChannel globalChannel,
        OutgoingTcpRequests outgoingTcpRequests) 
        : base(clients, privateChannels, globalChannel, outgoingTcpRequests)
      {
      }

      public override byte HandledType => 4;

      public override void Handle(int index, string identifier, ref NetworkRequest request)
      {
        ClientActiveChannelsRequest req = request.RequestData.Deserialize<ClientActiveChannelsRequest>();
        req.Channels = new List<ChannelOpenedRequest>(1);

        var globalChannel = GlobalChannel.GetChatOpenedRequest(identifier);

        req.Channels.Add(globalChannel);

        OutgoingRequests.Enqueue(index, req.ToMessage());
      }
    }
}
