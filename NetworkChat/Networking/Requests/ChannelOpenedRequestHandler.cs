﻿using System.Collections.Generic;
using NetworkChat.Logic;
using NetworkChat.Logic.Channels;
using NetworkChatTypes.Messages;
using NetworkChatTypes.Utils;

namespace NetworkChat.Networking.Requests
{
  internal sealed class ChannelOpenedRequestHandler : ServerRequestHandler
  {
    public ChannelOpenedRequestHandler(NetworkClients clients, PrivateChannels privateChannels, GlobalChannel globalChannel, OutgoingTcpRequests outgoingRequests) : base(clients, privateChannels, globalChannel, outgoingRequests)
    {
    }

    public override byte HandledType => 3;
    public override void Handle(int index, string identifier, ref NetworkRequest request)
    {
      ChannelOpenedRequest req = request.RequestData.Deserialize<ChannelOpenedRequest>();

      switch (req.TypeOfChannel)
      {
        case ChannelType.Private:
          OnPrivateChannelOpenRequest(req);
          break;
        case ChannelType.Global:
          break;
        case ChannelType.Group:
          break;
      }

    }

    private void OnPrivateChannelOpenRequest(ChannelOpenedRequest request)
    {
      int privateChannelIdentifier = PrivateChannels.GetPrivateChannel(request.ClientA, request.ClientB);
      int clientAIndex = Clients.GetIndexByIdentifier(request.ClientA);
      int clientBIndex = Clients.GetIndexByIdentifier(request.ClientB);

      var response = new ChannelOpenedRequest
      {
        ChannelIdentifier = privateChannelIdentifier,
        ClientA = request.ClientA,
        ClientB = request.ClientB,
        TypeOfChannel = ChannelType.Private,
        Messages = new List<string>(0)
      }.ToMessage();

      if (clientAIndex != -1)
      {
        OutgoingRequests.Enqueue(clientAIndex, response);
      }

      if (clientBIndex != -1)
      {
        OutgoingRequests.Enqueue(clientBIndex, response);
      }
        }
  }
}
