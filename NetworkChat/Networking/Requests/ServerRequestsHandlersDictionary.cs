﻿using System.Collections.Generic;

namespace NetworkChat.Networking.Requests
{
  public sealed class ServerRequestsHandlersDictionary
  {
    /// <summary>
    /// Handlers dictionary.
    /// </summary>
    private readonly IServerRequestHandler[] _requestHandlersDictionary = new IServerRequestHandler[10];

    /// <summary>
    /// Handlers initialization.
    /// </summary>
    /// <param name="serverRequestHandlers">List of all <see cref="IServerRequestHandler"/> implementations. This param is injected through Autofac.</param>
    public ServerRequestsHandlersDictionary(IList<IServerRequestHandler> serverRequestHandlers)
    {
      foreach (IServerRequestHandler handler in serverRequestHandlers)
      {
        _requestHandlersDictionary[handler.HandledType] = handler;
      }
    }

    public IServerRequestHandler this[byte requestIdentifier] => _requestHandlersDictionary[requestIdentifier];
  }
}
