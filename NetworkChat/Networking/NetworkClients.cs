﻿using System.Collections;
using System.Collections.Generic;
using NetworkChat.Configuration;
using NetworkChatTypes.Clients;

namespace NetworkChat.Networking
{
  /// <summary>
  /// Holds all the chat clients.
  /// </summary>
  internal class NetworkClients : IEnumerable<NetworkClientRecord>
  {
    private readonly NetworkClientRecord[] _clientRecords;

    public NetworkClientRecord[] Records => _clientRecords;

    private readonly NetworkClientIndexPool _pool;

    public NetworkClients(ChatConfiguration configuration, NetworkClientIndexPool pool)
    {
      _pool = pool;
      _clientRecords = new NetworkClientRecord[configuration.ServerCapacity];
    }

    public bool Add(string identifier, INetworkClient client)
    {
      int existsIndex = GetIndexByIdentifier(identifier);
      if (existsIndex != -1)
      {
        lock (_clientRecords)
        {
          _clientRecords[existsIndex].Client = client;
        }
        return true; // Do anything?
      }

      lock (_clientRecords)
      {
        if (!_pool.GetFirstAvailable(out int index))
        {
          return false;
        }

        _clientRecords[index] = new NetworkClientRecord { Client = client, StringClientIdentifier = identifier};
        return true;
      }
    }

    public int GetIndexByIdentifier(string identifier)
    {
      lock (_clientRecords)
      {
        for (int i = 0; i < _clientRecords.Length; i++)
        {
          if (_clientRecords[i].StringClientIdentifier == identifier)
          {
            return i;
          }
        }
      }
      return -1;
    }

    private bool Exists(string stringIdentifier)
    {
      if (string.IsNullOrEmpty(stringIdentifier))
        return false;

      return GetIndexByIdentifier(stringIdentifier) != -1;
    }

    public IEnumerable<int> ForEachIndex()
    {
      lock (_clientRecords)
      {
        for (int i = 0; i < _clientRecords.Length; i++)
        {
          if (_clientRecords[i].Client != null) yield return i;
        }
      }
    }

    public IEnumerator<NetworkClientRecord> GetEnumerator()
    {
      lock (_clientRecords)
      {
        for (int i = 0; i < _clientRecords.Length; i++)
        {
          if (_clientRecords[i].Client != null) yield return _clientRecords[i];
        }
      }
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }
  }

  internal struct NetworkClientRecord
  {
    public INetworkClient Client;
    public string StringClientIdentifier;
  }
}
