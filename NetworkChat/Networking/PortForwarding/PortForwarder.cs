﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using Open.Nat;

namespace NetworkChat.Networking.PortForwarding
{
    public static class NATUPnP
    {
      /// <summary>
      /// Remove port mapping on router.
      /// </summary>
      /// <param name="addresss">Leave it null if to use default (local) ip address.</param>
      /// <returns>Value indicating whether mapping was successfully removed.</returns>
      public static async Task<bool> RemoveMappingForIP(IPAddress addresss = null)
      {
        try
        {
          var nat = new NatDiscoverer();
          var cts = new CancellationTokenSource();
          cts.CancelAfter(5000);
          NatDevice device = await nat.DiscoverDeviceAsync(PortMapper.Upnp, cts).ConfigureAwait(false);
          IPAddress privateIP = addresss ?? await GetLocalIPAddress().ConfigureAwait(false);
          var mapping = await device.GetAllMappingsAsync().ConfigureAwait(false);

          foreach (Mapping m in mapping.Where(m => Equals(m.PrivateIP, privateIP)))
          {
              await device.DeletePortMapAsync(new Mapping(m.Protocol, m.PrivatePort, m.PublicPort));
          }
          return true;
        }
        catch
        {
          return false;
        }
      }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="publicPort">Port that you want to forward routed traffic to.
        /// For example if you opening TcpListener on port 6667 than you should pass this port as the first parameter.
        /// As result of this function you will receive public forwarded port that your client should use for connection to your listener.
        /// </param>
        /// <param name="protocol">TCP or UDP forwarding.</param>
        /// <param name="addresss">IPAddress for incoming routed traffic. Leave it null to create default port forwarding using local ip address.</param>
        /// <param name="mappingName">Mapping name that you can see in router settings.</param>
        /// <param name="cancelAfter">This function timeout in milliseconds. If we cannot forward port it will throw an error after this time.</param>
        /// <returns>Public port that your clients should use to connect to your port passed as "publicPort" parameter.</returns>
        public static async Task<int> CreateMapping(int publicPort, Protocol protocol = Protocol.Tcp, IPAddress addresss = null, string mappingName = "", int cancelAfter = 5000)
      {
          if (string.IsNullOrEmpty(mappingName))
          {
              mappingName = string.Format("Default mapping name ({0})", protocol.ToString());
          }
          var nat = new NatDiscoverer();
          var cts = new CancellationTokenSource();
          cts.CancelAfter(cancelAfter);

          NatDevice device = await nat.DiscoverDeviceAsync(PortMapper.Upnp, cts).ConfigureAwait(false);
          IPAddress privateIP = addresss ?? await GetLocalIPAddress().ConfigureAwait(false);
          var mapping = await device.GetAllMappingsAsync().ConfigureAwait(false);

          // Delete existing mapping. 
          foreach (Mapping m in mapping.Where(m => m.Protocol == protocol && Equals(m.PrivateIP, privateIP) && m.PrivatePort == publicPort))
          {
              await device.DeletePortMapAsync(new Mapping(m.Protocol, m.PrivatePort, m.PublicPort));
          }

        // Check if there is already existing mapping for given public port.
        // If so, increase result port until it remain unused yet.
          int port = 0;
          for (int i = publicPort; i < 65534; i++)
          {
              if (mapping.Any(m => m.PublicPort == i))
              {
                  continue;
              }
              port = i;
              break;
          }

          if (port == 0)
          {
              throw new Exception("Mapping failed: The available port was not found.");
          }
          await device.CreatePortMapAsync(new Mapping(protocol, publicPort, port, 0, mappingName));
          return port;
      }

      private static async Task<IPAddress> GetLocalIPAddress()
      {
          var host = await Dns.GetHostEntryAsync(Dns.GetHostName()).ConfigureAwait(false);
          foreach (var ip in host.AddressList)
          {
              if (ip.AddressFamily == AddressFamily.InterNetwork)
              {
                  return ip;
              }
          }
          return IPAddress.None;
      }
    }
}
