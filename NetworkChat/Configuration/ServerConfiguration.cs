﻿using System;
using System.Collections.Generic;
using PlayFab;

namespace NetworkChat.Configuration
{
  internal class ServerConfiguration
  {
    public ServerConfigurationData Data { get; private set; }

    public ServerConfigurationData InitConfiguration(ChatConfiguration chatConfiguration)
    {
      Data = new ServerConfigurationData();
      Data.ServerHostPort = chatConfiguration.DefaultPort;
      Data.IsPlayFab = chatConfiguration.IsPlayFab;
      Data.IsLocal = chatConfiguration.IsLocal;
      Data.ForwardPort = chatConfiguration.ForwardPort;

      if (Data.IsPlayFab && Data.IsLocal)
      {
        throw new Exception("App.config: \n" +
                            "ChatConfiguration section: \n" +
                            "IsPlayFab and IsLocal cannot be set up simultaneously, choose one of them.");
      }

      if (Data.IsPlayFab && Data.ForwardPort)
      {
        throw new Exception("App.config: \n" +
                            "ChatConfiguration section: \n" +
                            "You cannot set port forwarding for PlayFab configuration." +
                            "Hosting on the PlayFab custom server gives you forwarded port already.");
      }

      // If this is not playfab instance we don't expect configuration from command line.
      if (!Data.IsPlayFab) return Data;

      var commandLineArgs = System.Environment.GetCommandLineArgs();
      foreach (var arg in commandLineArgs)
      {
        var argArray = arg.Split('=');
        if (argArray.Length < 2)
        {
          continue;
        }
        var key = argArray[0].Contains("-") ? argArray[0].Replace("-", "").Trim() : argArray[0].Trim();
        var value = argArray[1].Trim();

        switch (key.ToLower())
        {
          case "game_id":
            ulong.TryParse(value, out Data.GameId);
            break;
          case "game_build_version":
            Data.GameBuildVersion = value;
            break;
          case "game_mode":
            Data.GameMode = value;
            break;
          case "server_host_domain":
            Data.ServerHostDomain = value;
            break;
          case "server_host_port":
            int.TryParse(value, out int hostPort);
            hostPort = hostPort > 0 ? hostPort : 7777;
            Data.ServerHostPort = hostPort;
            break;
          case "server_host_region":
            Data.ServerHostRegion = value;
            break;
          case "playfab_api_endpoint":
            Data.PlayFabApiEndpoint = value;
            break;
          case "title_secret_key":
            Data.TitleSecretKey = value;
            break;
          case "custom_data":
            Data.CustomData = value;
            break;
          case "log_file_path":
            Data.LogFilePath = value;
            break;
          case "output_files_directory_path":
            Data.OutputFilesDirectory = value;
            break;
          case "title_id":
            Data.TitleId = value;
            break;
        }
      }

      if (!string.IsNullOrEmpty(Data.TitleSecretKey))
      {
        PlayFabSettings.DeveloperSecretKey = Data.TitleSecretKey;
      }

      if (!string.IsNullOrEmpty(Data.TitleId))
      {
        PlayFabSettings.TitleId = Data.TitleId;
      }
      return Data;
    }
  }

  internal class ServerConfigurationData
  {
    public string TitleId;
    public ulong GameId;
    public string GameBuildVersion;
    public string GameMode;
    public string ServerHostDomain = "127.0.0.1";
    public int ServerHostPort = 6667;
    public string ServerHostRegion;
    public string PlayFabApiEndpoint;
    public string TitleSecretKey;
    public string CustomData;
    public bool IsCustomLogFile;
    public string LogFilePath;
    public string OutputFilesDirectory;
    public string BatchMode;
    public List<string> LogEntries;
    public bool CopyLogFiles;
    public bool IsPlayFab;
    public bool IsLocal;
    public bool ForwardPort;
  }
}
