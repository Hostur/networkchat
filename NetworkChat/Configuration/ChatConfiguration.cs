﻿using System;
using System.Configuration;

namespace NetworkChat.Configuration
{
  internal class ChatConfiguration : ConfigurationSection
  {
    /// <summary>
    /// Name of the key in App.config.
    /// Value indicate what the user global chat cooldown is in seconds.
    /// </summary>
    private const string GLOBAL_CHAT_PLAYER_COOLDOWN = "global_chat_cooldown";

    /// <summary>
    /// Name of the key in App.config.
    /// Value indicating whether build will be hosted by playfab custom server.
    /// </summary>
    private const string IS_PLAYFAB = "is_playfab";

    /// <summary>
    /// Name of the key in App.config.
    /// Value indicating whether build will be hosted by locally for development tests.
    /// </summary>
    private const string IS_LOCAL = "is_local";

    /// <summary>
    /// Name of the key in App.config.
    /// Value indicating default port for hosting this chat.
    /// If it is an PlayFab instance this port will be override by commands arguments in <see cref="ServerConfiguration"/>.
    /// </summary>
    private const string DEFAULT_PORT = "default_port";

    /// <summary>
    /// Name of the key in App.Config.
    /// Value indicating whether chat server should forward port.
    /// </summary>
    private const string FORWARD_PORT = "forward_port";

    /// <summary>
    /// Name of the key in App.config.
    /// </summary>
    private const string SERVER_CAPACITY = "capacity";

    /// <summary>
    /// Gets or sets global chat cooldown app identifier.
    /// </summary>
    [ConfigurationProperty(GLOBAL_CHAT_PLAYER_COOLDOWN, IsRequired = true)]
    public string GlobalChatCooldownString
    {
      get => (string)this[GLOBAL_CHAT_PLAYER_COOLDOWN];
      set => this[GLOBAL_CHAT_PLAYER_COOLDOWN] = value;
    }

    public int GlobalChatCooldown
    {
      get
      {
        if (!int.TryParse(GlobalChatCooldownString, out int value))
        {
          throw new Exception("Cannot parse ChatConfiguration.GlobalChatCooldownString into int.");
        }

        return value;
      }
    }

    /// <summary>
    /// Gets or sets value indicating whether this build instance is playfab custom server.
    /// </summary>
    [ConfigurationProperty(IS_PLAYFAB, IsRequired = true)]
    public bool IsPlayFab
    {
      get => (bool)this[IS_PLAYFAB];
      set => this[IS_PLAYFAB] = value;
    }

    /// <summary>
    /// Gets or sets value indicating whether this build instance is locally hosted for development purpose.
    /// </summary>
    [ConfigurationProperty(IS_LOCAL, IsRequired = true)]
    public bool IsLocal
    {
      get => (bool)this[IS_LOCAL];
      set => this[IS_LOCAL] = value;
    }

    /// <summary>
    /// Gets or sets default port value. If IsPlayFab = true than port will be override in <see cref="ServerConfiguration"/>.
    /// </summary>
    [ConfigurationProperty(DEFAULT_PORT, IsRequired = true)]
    public int DefaultPort
    {
      get => (int)this[DEFAULT_PORT];
      set => this[DEFAULT_PORT] = value;
    }

    /// <summary>
    /// Value indicating whether application should forward port.
    /// </summary>
    [ConfigurationProperty(FORWARD_PORT, IsRequired = true)]
    public bool ForwardPort
    {
      get => (bool)this[FORWARD_PORT];
      set => this[FORWARD_PORT] = value;
    }

    /// <summary>
    /// Server clients capacity.
    /// </summary>
    [ConfigurationProperty(SERVER_CAPACITY, IsRequired = true)]
    public int ServerCapacity
    {
      get => (int)this[SERVER_CAPACITY];
      set => this[SERVER_CAPACITY] = value;
    }
    }
}
