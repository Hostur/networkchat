﻿using System;
using System.Threading.Tasks;
using NetworkChat.Core;

namespace NetworkChat
{
  class Program
  {
    static void Main(string[] args)
    {
      ChatServer server = DI.Resolve<ChatServer>();
      int coreCount = 0;
      foreach (var item in new System.Management.ManagementObjectSearcher("Select * from Win32_Processor").Get())
      {
          coreCount += int.Parse(item["NumberOfCores"].ToString());
      }

      Task.Factory.StartNew(async () => await server.Start(coreCount).ConfigureAwait(false)).ConfigureAwait(false);
      Console.ReadKey();
      //Task.Run(async () => await server.Start(coreCount).ConfigureAwait(true));

      //server.Start();

      //int coreCount = 0;
      //foreach (var item in new System.Management.ManagementObjectSearcher("Select * from Win32_Processor").Get())
      //{
      //  coreCount += int.Parse(item["NumberOfCores"].ToString());
      //}
      //Console.WriteLine("Number Of Cores: {0}", coreCount);
      //      Console.WriteLine();
    }
    }
}
