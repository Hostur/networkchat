﻿namespace NetworkChat.Core
{
  internal interface IChatLogger
  {
    void Log(string msg);
    void LogError(string error);
  }
}
