﻿using System;

namespace NetworkChat.Core
{
  internal class LocalChatLogger : IChatLogger
  {
    public void Log(string msg)
    {
      Console.WriteLine(msg);
    }

    public void LogError(string error)
    {
      Console.WriteLine("[ERROR] " + error);
    }
  }
}
