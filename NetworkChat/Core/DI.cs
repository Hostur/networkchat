﻿using System;
using System.Configuration;
using Autofac;
using NetworkChat.Configuration;
using NetworkChat.Logic;
using NetworkChat.Logic.Channels;
using NetworkChat.Networking;
using NetworkChat.Networking.Requests;
using NetworkChat.Systems;

namespace NetworkChat.Core
{
  /// <summary>
  /// Dependency injection container which store all registered types.
  /// </summary>
  public static class DI
  {
    private static IContainer m_container;

    private static IContainer Container => m_container ?? (m_container = Initialize());

    public static T Resolve<T>()
    {
      return Container.Resolve<T>();
    }

    private static IContainer Initialize()
    {
      try
      {
        ContainerBuilder builder = new ContainerBuilder();

        RegisterRequestHandlers(builder);
        RegisterSingletones(builder);

        return builder.Build();
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
        throw;
      }
    }

    private static void RegisterRequestHandlers(ContainerBuilder builder)
    {
      builder.RegisterType<ClientActiveChannelsRequestHandler>()
        .As<IServerRequestHandler>()
        .SingleInstance();

      builder.RegisterType<DisconnectedRequestHandler>()
        .As<IServerRequestHandler>()
        .SingleInstance();

      builder.RegisterType<ChatMessagesHandler>()
        .As<IServerRequestHandler>()
        .SingleInstance();

      builder.RegisterType<ChannelOpenedRequestHandler>()
        .As<IServerRequestHandler>()
        .SingleInstance();
    }

    private static void RegisterSingletones(ContainerBuilder builder)
    {
      builder.Register(c => (ChatConfiguration)ConfigurationManager.GetSection("Chat/settings"))
        .As<ChatConfiguration>()
        .SingleInstance();

            builder.RegisterType<ServerConfiguration>()
        .AsSelf()
        .SingleInstance();

      builder.RegisterType<ChatServer>()
        .AsSelf()
        .SingleInstance();

      builder.RegisterType<LocalChatLogger>()
        .As<IChatLogger>()
        .SingleInstance();

      builder.RegisterType<NetworkClientIndexPool>()
        .AsSelf()
        .SingleInstance();

      builder.RegisterType<NetworkClients>()
        .AsSelf()
        .SingleInstance();

      builder.RegisterType<NetworkListener>()
        .AsSelf()
        .SingleInstance();

      builder.RegisterType<GenericServerRequestHandler>()
        .AsSelf()
        .SingleInstance();

      builder.RegisterType<ServerRequestsHandlersDictionary>()
        .AsSelf()
        .SingleInstance();

      builder.RegisterType<NetworkListeningSystem>()
        .AsSelf()
        .SingleInstance();

      builder.RegisterType<NetworkSendingSystem>()
        .AsSelf()
        .SingleInstance();

      builder.RegisterType<OutgoingTcpRequests>()
        .AsSelf()
        .SingleInstance();


      builder.RegisterType<GlobalChannel>()
        .AsSelf()
        .SingleInstance();

      builder.RegisterType<PrivateChannels>()
        .AsSelf()
        .SingleInstance();
    }
  }
}
