﻿// If you want to use protobuf serialization notice that is not supported by IL2CPP.
// So if your clients using mobile builds avoid using protobuf. Also you have to download prorobuf-net from nuget.
#if PROTOBUF
using System.IO;
using NetworkChatTypes.Messages;
using ProtoBuf;

namespace NetworkChatTypes.Utils
{
    public static class ProtoSerializer
    {
        public static byte[] Serialize<T>(this T data) where T : struct
        {
            using (var stream = new MemoryStream())
            {
                Serializer.Serialize(stream, data);
                return stream.ToArray();
            }
        }

        public static byte[] SerializeArray<T>(this T[] data) where T : struct
        {
            using (var stream = new MemoryStream())
            {
                Serializer.Serialize(stream, data);
                return stream.ToArray();
            }
        }

        public static T Deserialize<T>(this byte[] data) where T : struct
        {
            using (var stream = new MemoryStream(data))
            {
                return Serializer.Deserialize<T>(stream);
            }
        }

        public static T[] DeserializeArray<T>(this byte[] data) where T : struct
        {
            using (var stream = new MemoryStream(data))
            {
                return Serializer.Deserialize<T[]>(stream);
            }
        }

        public static byte[] ToRequestsArray<T>(this T obj) where T : struct, INetworkMessageProvider
        {
            NetworkRequest request = obj.ToMessage();
            return SerializeArray(new[] { request });
        }

        public static byte[] ToRequest<T>(this T request) where T : struct, INetworkMessageProvider
        {
            return Serialize(request.ToMessage());
        }

        public static byte[] ToRequestsArray(this INetworkMessageProvider obj)
        {
            NetworkRequest request = obj.ToMessage();
            return SerializeArray(new[] { request });
        }

        public static byte[] ToRequest(this INetworkMessageProvider request)
        {
            return Serialize(request.ToMessage());
        }
    }
}
#endif