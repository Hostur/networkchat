﻿using System;
using System.Net.Sockets;

namespace NetworkChatTypes.Utils
{
  internal static class TcpClientExtension
  {
    private const int PREFIX = 4;
    private static readonly byte[] _prefixTemplate = new byte[PREFIX];

    public static byte[] ReceiveFromTcp(this TcpClient client)
    {
      byte[] receivePrefix = _prefixTemplate;
      int receiveTotal = 0;

      int receive = client.Client.Receive(receivePrefix, 0, 4, 0);
      int receiveSize = BitConverter.ToInt32(receivePrefix, 0);
      int receiveDataLeft = receiveSize;
      byte[] data = new byte[receiveSize];
      while (receiveTotal < receiveSize)
      {
        receive = client.Client.Receive(data, receiveTotal, receiveDataLeft, 0);
        if (receive == 0)
        {
          break;
        }
        receiveTotal += receive;
        receiveDataLeft -= receive;
      }
      return data;
    }

    public static void SendTcp(this TcpClient client, ref byte[] data)
    {
      int sendingSize = data.Length;
      int sendingDataLeft = sendingSize;
      var sendPrefix = BitConverter.GetBytes(sendingSize);
      int sendingSent = client.Client.Send(sendPrefix);
      int sendingTotal = 0;

      while (sendingTotal < sendingSize)
      {
        sendingSent = client.Client.Send(data, sendingTotal, sendingDataLeft, SocketFlags.None);
        sendingTotal += sendingSent;
        sendingDataLeft -= sendingSent;
      }
    }
  }
}