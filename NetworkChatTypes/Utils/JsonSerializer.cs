﻿#if !PROTOBUF
using System.Text;
using NetworkChatTypes.Messages;
using Newtonsoft.Json;

namespace NetworkChatTypes.Utils
{
    public static class JsonSerializer
    {
      public static byte[] Serialize<T>(this T data) where T : struct
      {
        var str = JsonConvert.SerializeObject(data);
        return Encoding.ASCII.GetBytes(str);        
      }

      //public static byte[] SerializeArray<T>(this T[] data) where T : struct
      //{
      //  var str = JsonConvert.SerializeObject(data);
      //  return Encoding.ASCII.GetBytes(str);
      //}

      public static T Deserialize<T>(this byte[] data) where T : struct
      {
        string str = Encoding.ASCII.GetString(data);
        return JsonConvert.DeserializeObject<T>(str);
      }

      //public static T[] DeserializeArray<T>(this byte[] data) where T : struct
      //{
      //  string str = Encoding.ASCII.GetString(data);
      //  return JsonConvert.DeserializeObject<T[]>(str);
      //}

      //public static byte[] ToRequestsArray<T>(this T obj) where T : struct, INetworkMessageProvider
      //{
      //  NetworkRequest request = obj.ToMessage();
      //  return SerializeArray(new[] { request });
      //}

      public static byte[] ToRequest<T>(this T request) where T : struct, INetworkMessageProvider
      {
        return Serialize(request.ToMessage());
      }

      //public static byte[] ToRequestsArray(this INetworkMessageProvider obj)
      //{
      //  NetworkRequest request = obj.ToMessage();
      //  return SerializeArray(new[] {request});
      //}
    }
}
#endif