﻿using NetworkChatTypes.Utils;
using ProtoBuf;

namespace NetworkChatTypes.Messages
{
    [ProtoContract]
    public struct DisconnectedRequest : INetworkMessageProvider
    {
      public const byte Type = 5;

      public NetworkRequest ToMessage()
      {
        return new NetworkRequest(Type, this.Serialize());
      }
    }
}
