﻿using ProtoBuf;

namespace NetworkChatTypes.Messages
{
  [ProtoContract]
  public struct NetworkRequest
  {
    [ProtoMember(1)]
    public byte RequestType;

    [ProtoMember(2)]
    public byte[] RequestData;

    public NetworkRequest(byte type, byte[] data)
    {
      RequestType = type;
      RequestData = data;
    }
  }
}
