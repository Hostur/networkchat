﻿namespace NetworkChatTypes.Messages
{
  public interface INetworkMessageProvider
  {
    NetworkRequest ToMessage();
  }
}
