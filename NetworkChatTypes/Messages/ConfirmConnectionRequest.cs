﻿using NetworkChatTypes.Utils;
using ProtoBuf;

namespace NetworkChatTypes.Messages
{
  [ProtoContract]
  public struct ConfirmConnectionRequest : INetworkMessageProvider
  {
    public const byte Type = 6;

    public NetworkRequest ToMessage()
    {
      return new NetworkRequest(Type, this.Serialize());
    }
  }
}
