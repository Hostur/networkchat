﻿using System.Collections.Generic;
using NetworkChatTypes.Utils;
using ProtoBuf;

namespace NetworkChatTypes.Messages
{
  /// <summary>
  /// Type = 4.
  /// </summary>
  [ProtoContract]
  public struct ClientActiveChannelsRequest : INetworkMessageProvider
  {
    [ProtoMember(1)]
    public List<ChannelOpenedRequest> Channels;

    public const byte Type = 4;

    public NetworkRequest ToMessage()
    {
      return new NetworkRequest(Type, this.Serialize());
    }
  }
}
