﻿using ProtoBuf;

namespace NetworkChatTypes.Messages
{
  /// <summary>
  /// Chat channel is connection between two players or a player and a group.
  /// </summary>
  [ProtoContract]
  public struct ChatChannel
  {
    /// <summary>
    /// Channel name is other player string identifier, or a group name.
    /// </summary>
    [ProtoMember(1)]
    public string ChannelIdentifier;

    /// <summary>
    /// Message type defined by a target channel.
    /// </summary>
    [ProtoMember(2)]
    public ChannelType ChannelType;
  }
}
