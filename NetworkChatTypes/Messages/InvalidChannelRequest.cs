﻿using NetworkChatTypes.Utils;
using ProtoBuf;

namespace NetworkChatTypes.Messages
{
  [ProtoContract]
  public struct InvalidChannelRequest : INetworkMessageProvider
  {
    [ProtoMember(1)]
    public int ChannelIdentifier;

    public InvalidChannelRequest(int channelIdentifier)
    {
      ChannelIdentifier = channelIdentifier;
    }

    public const byte Type = 7;

    public NetworkRequest ToMessage()
    {
      return new NetworkRequest(Type, this.Serialize());
    }
  }
}
