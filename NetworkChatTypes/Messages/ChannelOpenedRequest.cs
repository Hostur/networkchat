﻿using System.Collections.Generic;
using NetworkChatTypes.Utils;
using ProtoBuf;

namespace NetworkChatTypes.Messages
{
  /// <summary>
  /// This request is an server response for ClientA and ClientB when any of these players sends <see cref="OpenChannelConnectionRequest"/>.
  /// Type = 3. 
  /// </summary>
  [ProtoContract]
  public struct ChannelOpenedRequest : INetworkMessageProvider
  {
    [ProtoMember(1)]
    public int ChannelIdentifier;

    [ProtoMember(2)]
    public string ClientA;

    [ProtoMember(3)]
    public string ClientB;

    [ProtoMember(4)]
    public ChannelType TypeOfChannel;

    [ProtoMember(5)]
    public List<string> Messages;

    public const byte Type = 3;

    public NetworkRequest ToMessage()
    {
      return new NetworkRequest(Type, this.Serialize());
    }
  }
}
