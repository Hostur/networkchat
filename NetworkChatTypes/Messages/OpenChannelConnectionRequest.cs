﻿using NetworkChatTypes.Utils;
using ProtoBuf;

namespace NetworkChatTypes.Messages
{
  /// <summary>
  /// This request is used by a client top subscribe for a messages from given channel.
  /// Type = 1.
  /// </summary>
  [ProtoContract]
  public struct OpenChannelConnectionRequest : INetworkMessageProvider
  {
    [ProtoMember(1)]
    public ChatChannel Channel;

    public OpenChannelConnectionRequest(ChatChannel channel) { Channel = channel; }

    public const byte Type = 1;

    public NetworkRequest ToMessage()
    {
      return new NetworkRequest(Type, this.Serialize());
    }
  }
}
