﻿using NetworkChatTypes.Utils;
using ProtoBuf;

namespace NetworkChatTypes.Messages
{
  /// <summary>
  /// Main structure used for network communication. Each network chat message is an instance of this structure.
  /// Type = 0;
  /// </summary>
  [ProtoContract]
  public struct ChatMessage : INetworkMessageProvider
  {
    /// <summary>
    /// You can get ChannelIdentifier by sending <see cref="OpenChannelConnectionRequest"/>.
    /// Its received as <see cref="ChannelOpenedRequest"/>.
    /// </summary>
    [ProtoMember(1)]
    public int ChannelIdentifier;

    [ProtoMember(2)]
    public string Message;

    [ProtoMember(3)]
    public ChannelType TypeOfChannel;

    public const byte Type = 0;

    public NetworkRequest ToMessage()
    {
      return new NetworkRequest(Type, this.Serialize());
    }
  }
}
