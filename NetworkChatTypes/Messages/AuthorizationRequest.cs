﻿using NetworkChatTypes.Utils;
using ProtoBuf;

namespace NetworkChatTypes.Messages
{
  /// <summary>
  /// Server sends this request empty as initial handshake for client.
  /// In response client should assign this request fields to introduce himself.
  /// Type = 2.
  /// </summary>
  [ProtoContract]
  public struct AuthorizationRequest : INetworkMessageProvider
  {
    [ProtoMember(1)]
    public string StringClientIdentifier;

    [ProtoMember(2)]
    public string GameIdentifier;

    [ProtoMember(3)]
    public Status Status;

    /// <summary>
    /// This ticket is provided by client to server.
    /// Client may get if from playfab api by calling ClientAPI matchmake.
    /// </summary>
    [ProtoMember(4)]
    public string AuthenticationTicket;

    public const byte Type = 2;

    public NetworkRequest ToMessage()
    {
      return new NetworkRequest(Type, this.Serialize());
    }
  }

  public enum Status
  {
    ShouldConfirm,
    ConfirmedSuccessfully,
    ConfirmedFailed
  }
}
