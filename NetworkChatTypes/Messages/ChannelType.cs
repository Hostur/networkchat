﻿using System;

namespace NetworkChatTypes.Messages
{
  /// <summary>
  /// Each <see cref="ChatMessage"/> should is defined by a channel type to recognize what to do with this message after receive.
  /// </summary>
  [Serializable]
  public enum ChannelType
  {
    /// <summary>
    /// Private message between two clients.
    /// </summary>
    Private,

    /// <summary>
    /// Message inside a group or guild.
    /// </summary>
    Group,

    /// <summary>
    /// Global message if supported.
    /// </summary>
    Global
  }
}
