﻿using System.Collections.Generic;
using NetworkChatTypes.Messages;

namespace NetworkChatTypes.ClientChatProxy
{
    public struct ClientChatChannel
    {
      /// <summary>
      /// Identifier which is provided by server.
      /// </summary>
      internal int ChannelIdentifier { get; set; }

      /// <summary>
      /// Other player string identifier or guild name depends of channel type.
      /// </summary>
      public string OtherClientIdentifier { get; }

      /// <summary>
      /// Value indicating whether this channel is related to other player private conversation or a guild.
      /// </summary>
      public ChannelType TypeOfChannel { get; }

      /// <summary>
      /// List of messages.
      /// </summary>
      public List<string> Messages { get; internal set; }

      /// <summary>
      /// This constructor is used when we received ChannelOpenedRequest from the server and we don't have
      /// already opened channel with this "other" client.
      /// </summary>
      /// <param name="channelIdentifier">Channel identifier provided by the server.</param>
      /// <param name="otherClientIdentifier">Other client identifier.</param>
      /// <param name="channelType">Channel type that indicating which who we are talking to.</param>
      /// <param name="messages">List of messages from the server. It can be the first message from other player or channel history for example.</param>
      internal ClientChatChannel(int channelIdentifier, string otherClientIdentifier, ChannelType channelType,
        List<string> messages)
      {
        ChannelIdentifier = channelIdentifier;
        OtherClientIdentifier = otherClientIdentifier;
        TypeOfChannel = channelType;
        Messages = messages;
      }

      /// <summary>
      /// This constructor is used when client opening new conversation but it is not yet validated by server
      /// so there is no valid channel identifier.
      /// </summary>
      /// <param name="otherClientIdentifier">Other entity that we want sent message to. It can be other player identifier or guild name.</param>
      /// <param name="typeOfChannel">Channel type that indicating whether we want to send message to other player, guild or global.</param>
      internal ClientChatChannel(string otherClientIdentifier, ChannelType typeOfChannel)
      {
        ChannelIdentifier = -1;
        OtherClientIdentifier = otherClientIdentifier;
        TypeOfChannel = typeOfChannel;
        Messages = new List<string>(50);
      }
    }
}
