﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NetworkChatTypes.Clients;
using NetworkChatTypes.Logic.Requests;
using NetworkChatTypes.Messages;
using NetworkChatTypes.Utils;
using UnityEngine;

namespace NetworkChatTypes.ClientChatProxy
{
  internal sealed class ClientNetworkChat : IClientNetworkChat
  {
    public Action<ClientChatChannel> OnChatChannelOpened { get; set; }
    public Action<ClientChatChannel> OnMessageReceived { get; set; }

    public Action OnChatClientAuthorized { get; set; }

#region Do not touch it.
    public bool Initialized { get; private set; }
    private string _gameIdentifier = null;
    private string _ourClientStringIdentifier = null;
    private string _authenticationTicket = null;
    private INetworkClient _networkClient;
    private OutgoingNetworkRequestsQueue _outgoingNetworkRequestsQueue;
    private readonly Thread _chatThread;
#endregion
    public List<ClientChatChannel> Channels { get; private set; }

    internal ClientNetworkChat()
    {
      Channels = new List<ClientChatChannel>(4);
      _chatThread = new Thread(ProceedNetworkChat) { IsBackground = true };
      _outgoingNetworkRequestsQueue = new OutgoingNetworkRequestsQueue(12);
    }

    public async Task<bool> InitializeNetworkChatClient(
      string gameIdentifier,
      string ourClientStringIdentifier,
      string ipAddress,
      int port,
      string authenticationTicket)
    {
      _authenticationTicket = authenticationTicket;
      _gameIdentifier = gameIdentifier;
      _ourClientStringIdentifier = ourClientStringIdentifier;
     
      TcpClient client = new TcpClient(ipAddress, port);
      _networkClient = NetworkClientsFactory.Create(client);

      try
      {
        // Opens connection.
        client.GetStream();
        // Set initialized if no error occur.
        Initialized = true;
        // Start client chat thread.
        _chatThread.Start();
      }
      catch
      {
        return false;
      }

      return true;
    }

    private void ProceedNetworkChat()
    {
      while (Initialized)
      {
        try
        {
          while (_networkClient.Receive(out byte[] result))
          {
            if (result.Length == 0)
            {
              continue;
            }
            
            UnityEngine.Debug.LogError("Received byte array. Now try to deserialize.");
            NetworkRequest request = result.Deserialize<NetworkRequest>();
            UnityEngine.Debug.LogError("Deserialized request!");

            UnityEngine.Debug.Log("Incoming request: " + request.RequestType);
            HandleRequest(ref request);
          }

          if (_outgoingNetworkRequestsQueue.TryDequeueRequests(out NetworkRequest[] requests))
          {
            foreach (NetworkRequest networkRequest in requests)
            {
              _networkClient.Send(networkRequest.Serialize());
            }
          }
          Thread.Sleep(1000);
        }
        catch (Exception e)
        {
          throw new Exception("Error occur on IClientNetworkChat thread: " + e.Message);
        }
      }
    }

    private void HandleRequest(ref NetworkRequest req)
    {
      UnityEngine.Debug.LogError("Incoming request: " + req.RequestType);
      switch (req.RequestType)
      {
                case 0:
                  break;
                case 1:
                  break;
                case 2:
                  OnAuthorizationRequestHandler(ref req);
                  break;
                case 3:
                  break;
                case 4:
                  OnMyOpenChannelsRequestHandler(ref req);
                  break;
                case 5:
                  break;
                case 6:
                  OnConfirmConnectionRequestHandler();
                  break;
                case 7:
                  OnInvalidChannelRequest();
                  break;
      }
    }

    private void OnInvalidChannelRequest()
    {

    }

    private void OnConfirmConnectionRequestHandler()
    {
      _outgoingNetworkRequestsQueue.EnqueueRequest(new ConfirmConnectionRequest().ToMessage());
    }

    private void OnMyOpenChannelsRequestHandler(ref NetworkRequest req)
    {
      ClientActiveChannelsRequest request = req.RequestData.Deserialize<ClientActiveChannelsRequest>();
      foreach (ChannelOpenedRequest channelOpenedRequest in request.Channels)
      {
        OnChannelOpenedRequestHandler(channelOpenedRequest);
      }
      OnChatClientAuthorized?.Invoke();
    }

    private void OnChannelOpenedRequestHandler(ChannelOpenedRequest request)
    {
      string otherIdentifier = (request.ClientA == _ourClientStringIdentifier) ? request.ClientB : request.ClientA;

      // It there is no such channel opened create a new one and invoke callback with new channel.
      if (!Channels.Any(c => c.ChannelIdentifier != -1 && c.ChannelIdentifier == request.ChannelIdentifier))
      {
        var channel = new ClientChatChannel(request.ChannelIdentifier, otherIdentifier, request.TypeOfChannel,
          request.Messages);
        Channels.Add(channel);
        OnChatChannelOpened?.Invoke(channel);
      }
    }

    /// <summary>
    /// Authorization request handler.
    /// </summary>
    private void OnAuthorizationRequestHandler(ref NetworkRequest req)
    {
      UnityEngine.Debug.LogError("Deserialize AuthoruzationRequest..");
      if(req.RequestData == null || req.RequestData.Length == 0) Debug.LogError("reqeuest data is null");

      UnityEngine.Debug.LogError(Encoding.ASCII.GetString(req.RequestData));
      AuthorizationRequest request = req.RequestData.Deserialize<AuthorizationRequest>();
      UnityEngine.Debug.LogError("DeserializeD AuthoruzationRequest..");
     if (request.Status == Status.ShouldConfirm)
      {
        request.GameIdentifier = _gameIdentifier;
        request.StringClientIdentifier = _ourClientStringIdentifier;
        request.AuthenticationTicket = _authenticationTicket;
        _outgoingNetworkRequestsQueue.EnqueueRequest(request.ToMessage());
      }
      else if (request.Status == Status.ConfirmedSuccessfully)
      {
        _outgoingNetworkRequestsQueue.EnqueueRequest(new ClientActiveChannelsRequest().ToMessage());
      }
      else if (request.Status == Status.ConfirmedFailed)
      {
        throw new Exception("IClientNetworkChat connected but confirmed failed.");
      }
    }

    public void OpenChannel(string otherClientStringIdentifier, ChannelType typeOfConversation)
    {
      _outgoingNetworkRequestsQueue.EnqueueRequest(new OpenChannelConnectionRequest(new ChatChannel
      {
        ChannelIdentifier = otherClientStringIdentifier,
        ChannelType = typeOfConversation
      }).ToMessage());
    }

    public void Send(ClientChatChannel channel, string message)
    {
      _outgoingNetworkRequestsQueue.EnqueueRequest(new ChatMessage
      {
        Message = message,
        ChannelIdentifier = channel.ChannelIdentifier
      }.ToMessage());
    }

    public void ShutDown()
    {
      Initialized = false;
      _networkClient.Send(new DisconnectedRequest().ToRequest());
      _networkClient.Dispose();
    }

    public void Dispose()
    {
      Initialized = false;
      _networkClient.Dispose();
    }
  }
}
