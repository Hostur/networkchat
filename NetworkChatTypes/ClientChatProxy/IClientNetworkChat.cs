﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NetworkChatTypes.Messages;

namespace NetworkChatTypes.ClientChatProxy
{
  /// <summary>
  /// The only interface that should be used on the Client side of the chat system.
  /// </summary>
  public interface IClientNetworkChat : IDisposable
  {
    Action<ClientChatChannel> OnChatChannelOpened { get; set; }

    Action<ClientChatChannel> OnMessageReceived { get; set; }

    Action OnChatClientAuthorized { get; set; }

    /// <summary>
    /// Value indicating whether this client is initialized.
    /// </summary>
    bool Initialized { get; }

    /// <summary>
    /// Open client to server connection.
    /// </summary>
    /// <param name="gameIdentifier">Game identifier.</param>
    /// <param name="ourClientStringIdentifier">Out client string identifier.</param>
    /// <returns>Value indicating whether we successfully connected to the server.</returns>
    Task<bool> InitializeNetworkChatClient(
      string gameIdentifier,
      string ourClientStringIdentifier,
      string ipAddress,
      int port,
      string authenticationTicket);

    /// <summary>
    /// List of active channels.
    /// </summary>
    List<ClientChatChannel> Channels { get; }

    ///// <summary>
    ///// This function requests the server for your active channels.
    ///// It can be called when entering the game hub for example.
    ///// As result it opens all the currently active channels with up to date messages content.
    ///// It is quite expensive request so be careful calling it.
    ///// </summary>
    //Task GetMyActiveChannelsFromTheServer();

    /// <summary>
    /// Sends open channel request to the server.
    /// This is asynchronous operation that comes back in a form of OnChatChannelOpened callback.
    /// </summary>
    /// <param name="otherClientStringIdentifier">Entity that we want to talk to.</param>
    /// <param name="typeOfConversation">Channel type indicating whether is private, guild or global conversation.</param>
    void OpenChannel(string otherClientStringIdentifier, ChannelType typeOfConversation);

    /// <summary>
    /// Sends message to the 
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="message"></param>
    void Send(ClientChatChannel channel, string message);

    /// <summary>
    /// Kill network chat thread.
    /// </summary>
    void ShutDown();

  }

  public static class ClientNetworkChatFactory
  {
    public static IClientNetworkChat Create() => new ClientNetworkChat();
  }
}
