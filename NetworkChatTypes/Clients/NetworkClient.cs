﻿using System.Net.Sockets;
using NetworkChatTypes.Utils;

namespace NetworkChatTypes.Clients
{
  internal class NetworkClient : INetworkClient
  {
    private readonly TcpClient _tcpClient;
    private NetworkStream _networkStream;

    public NetworkClient(TcpClient tcpClient)
    {
      _tcpClient = tcpClient;
    }

    bool INetworkClient.IsConnected => _tcpClient.Connected;

    bool INetworkClient.Receive(out byte[] result)
    {
      if (!IsStreamAvailable())
      {
        result = null;
        return false;
      }

      result = _tcpClient.ReceiveFromTcp();
      return result?.Length > 0;
    }

    void INetworkClient.Send(byte[] data)
    {
      if (!_tcpClient.Connected) return;
      _tcpClient.SendTcp(ref data);
    }

    private bool IsStreamAvailable()
    {
      if (!_tcpClient.Connected)
      {
        return false;
      }
      _networkStream = _tcpClient.GetStream();
      return _networkStream.DataAvailable;
    }

    public void Dispose()
    {
      _tcpClient?.Dispose();
      _networkStream?.Dispose();
    }
  }
}
