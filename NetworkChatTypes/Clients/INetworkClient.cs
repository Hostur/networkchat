﻿using System;
using System.Net.Sockets;

namespace NetworkChatTypes.Clients
{
  public interface INetworkClient : IDisposable
  {
    bool IsConnected { get; }
    bool Receive(out byte[] result);
    void Send(byte[] data);
  }

  public static class NetworkClientsFactory
  {
    public static INetworkClient Create(TcpClient client) => new NetworkClient(client);
  }
}
