﻿using System.Collections.Generic;
using NetworkChatTypes.Messages;

namespace NetworkChatTypes.Logic.Requests
{
  public struct OutgoingNetworkRequestsQueue
  {
    private readonly List<NetworkRequest> _outgoingRequests;

    public OutgoingNetworkRequestsQueue(int queueCapacity)
    {
      _outgoingRequests = new List<NetworkRequest>(queueCapacity);
    }

    public void Clear()
    {
      lock (_outgoingRequests)
      {
        _outgoingRequests.Clear();
      }
    }

    public void EnqueueRequest(NetworkRequest request)
    {
      lock (_outgoingRequests)
      {
        _outgoingRequests.Add(request);
      }
    }

    public bool TryDequeueRequests(out NetworkRequest[] result)
    {
      lock (_outgoingRequests)
      {
        if (_outgoingRequests.Count > 0)
        {
          result = _outgoingRequests.ToArray();
          _outgoingRequests.Clear();
          return true;
        }
      }
      result = new NetworkRequest[0];
      return false;
    }
  }
}
